xml.instruct!
 
xml.rss "version" => "2.0", "xmlns:dc" => "http://purl.org/dc/elements/1.1/" do
  xml.channel do
 
    xml.title       "Feed Name"
    xml.link        url_for :only_path => false, :controller => 'posts'
    xml.pubDate     CGI.rfc1123_date @posts.first.updated_at if @posts.any?
    xml.description "Feed Description"
 
    @posts.each do |posts|
      xml.item do
        xml.title       post.name
        xml.link        url_for :only_path => false, :controller => 'posts', :action => 'show', :id => post.id
        xml.description post.body
        xml.pubDate     CGI.rfc1123_date post.updated_at
        xml.guid        url_for :only_path => false, :controller => 'posts', :action => 'show', :id => post.id
        xml.author      "#{post.author.email} (#{post.author.name})"
      end
    end
 
  end
end